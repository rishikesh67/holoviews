# Holoviews

> Analysis and visualization

### Let's start

An **open source** python library for data analytics and visualization.

Getting started - http://holoviews.org/getting_started/index.html

User guide - http://holoviews.org/user_guide/index.html

Installing and configuring Holovies - http://holoviews.org/user_guide/Installing_and_Configuring.html

Gallery - http://holoviews.org/gallery/index.html

Reference gallery - http://holoviews.org/reference/index.html

### Let's try this now

Introduction - http://holoviews.org/getting_started/Introduction.html

### Usage

> conda install -c ioam holoviews bokeh
> conda install matplotlib plotly
> pip install 'holoviews[recommended]'

> jupyter notebook --generate-config

> jupyter notebook --NotebookApp.iopub_data_rate_limit=100000000

Lauches Jupyter notebook

### Quick start

> holoviews --install-examples
> cd holoviews-examples
> mkdir practice

Now, create one python file named `01_read_csv.py` and save it inside `practice` with the below code.

```python
	import pandas as pd
	import numpy as np
	import holoviews as hv


	hv.extension('bokeh')

	# Reading CSV file
	station_info = pd.read_csv('../assets/station_info.csv')

	print "\n........STATION INFORMATION:-"
	print station_info;

	print "\n........STATION INFORMATION HEAD:-"
	print station_info.head()
```

> cd practice
> python 01_read_csv.py

