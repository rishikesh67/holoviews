
# coding: utf-8

# # Jupyter Notebook 
# 
# A great tool for develpoers who work on Python, R, Julia for Data analysis and other research tasks.
# 
# ***
# ***Written by: *** *Rishikesh Agrawani, Python/Django developer*
# 
# ***Last updated:*** *08 April 2018, Sun*
# ***

# In[2]:


# List of even numbers
evens = [2, 4, 6, 8, 22, 0, 10];
print evens;


# In[16]:


odds = [1, 7, 9, 11, 13, 19, 77]
odds 


# In[5]:


print "Welcome to Jupyter Notebook";


# In[17]:


commands = {"RUN": "Ctrl + ENTER", "OPEN COMMAND PALETTE": "Ctrl + Shift + p"};

print "JUPYTER NOTEBOOK:-"

for description, command in commands.iteritems():
    print description, ": ",command
    
commands


# In[33]:


import numpy as np
from matplotlib import pyplot as plt

x = np.linspace(0, 2*np.pi, 100)
print x
# x
y = np.sin(x)
print y

get_ipython().magic(u'matplotlib inline')
plt.title('Sine wave')
plt.plot(x, y)


# In[34]:


# filtering even elements from a list of mixed numbers
mixed_nums = [1, 5, 0, 34, 67, 98, 55, 32, 12, 45, 67, 89, 10, 23, 44]
print "Mixed numbers:", mixed_nums;

evens = [num for num in mixed_nums if num % 2 == 0];
print "Even numbers:", evens;


# ***
# Thanks.
