"""
	{
		HOLOVIEWS, BOKEH
	}
"""

import pandas as pd
import numpy as np
import holoviews as hv


hv.extension('bokeh')

# Reading CSV file
station_info = pd.read_csv('../assets/station_info.csv')

print "\n........STATION INFORMATION:-"
print station_info;

print "\n........STATION INFORMATION HEAD:-"
print station_info.head()
