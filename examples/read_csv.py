"""
	{
		HOLOVIEWS, BOKEH
	}
"""

import pandas as pd
import numpy as np
import holoviews as hv


hv.extension('bokeh')

# Reading CSV file
calendar_event = pd.read_csv('./csvs/C2ImportCalEventSample.csv')

print "\n........CALENDAR EVENT:-"
print calendar_event;

print "\n........CALENDAR EVENT HEAD:-"
print calendar_event.head()

